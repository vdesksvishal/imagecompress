﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace CompressImage.Web.DAL
{
	public class DataMethods
	{
		public void Insert(string fileName, string path, int size, DateTime uploadDt)
		{
			string cs = ConfigurationManager.ConnectionStrings["sqlConnectionString"].ConnectionString;
			using (SqlConnection conn = new SqlConnection(cs))
			{
				conn.Open();

				string sql = "insert into tblImages (fileName, filePath, dt_upload) values(@fileName, @filePath, @dt_upload)";
				SqlCommand cmd = new SqlCommand(sql, conn);
				cmd.Parameters.Add(new SqlParameter("@fileName", fileName));
				cmd.Parameters.Add(new SqlParameter("@filePath", path));
				cmd.Parameters.Add(new SqlParameter("@dt_upload", uploadDt));

				cmd.ExecuteNonQuery();
				conn.Close();
			}
		}
	}
}