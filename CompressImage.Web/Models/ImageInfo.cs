﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CompressImage.Web.Models
{
	public class ImageInfo
	{
		public string ImageName { get; set; }

		public string ImagePath { get; set; }

		public string ImageSizeInfo { get; set; }

		public string Savings { get; set; }

		public DateTime UploadedDt { get; set; }
	}
}