﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CompressImage.Web.DAL;
using NLog;
using System.Drawing.Imaging;
using System.Configuration;
using CompressImage.Web.Models;
using System.Net;
using nQuant;
namespace CompressImage.Web.Controllers
{
	public class HomeController : Controller
	{
		private static Logger logger = LogManager.GetCurrentClassLogger();
		public ActionResult Index()
		{
				return View();
		}

	
		[HttpPost]
		public ActionResult Upload(HttpPostedFileBase file)
		{

			if (Request.Files.Count > 0)
			{
				foreach (string uploadFile in Request.Files)
				{
					var _file = Request.Files[uploadFile];
					break;
				}
			}


			return View();
		}

		[HttpPost]

		public ActionResult UploadFile(HttpPostedFileBase file)
		{
			try
			{
				if (file.ContentLength > 0)
				{
					string _FileName = Path.GetFileName(file.FileName);
					string _path = Path.Combine(Server.MapPath("~/UploadedFiles"), _FileName);
					file.SaveAs(_path);

					
					DataMethods dm = new DataMethods();
					dm.Insert(_FileName, _path, file.ContentLength, DateTime.Now);

					Stream strm = file.InputStream;
					_FileName = string.Concat(Path.GetFileNameWithoutExtension(file.FileName), "_Compressed", Path.GetExtension(file.FileName));

					var targetFile = Path.Combine(Server.MapPath("~/UploadedFiles"), _FileName);

					string ext = Path.GetExtension(file.FileName).ToLower();
					ImageCodecInfo jpgEncoder = GetEncoder(ImageFormat.Bmp);
					if (ext.Contains("jpeg") || ext.Contains("jpg"))
						jpgEncoder = GetEncoder(ImageFormat.Jpeg);
					else if(ext.Contains("png"))
						jpgEncoder = GetEncoder(ImageFormat.Png);
					else if (ext.Contains("ico"))
						jpgEncoder = GetEncoder(ImageFormat.Icon);

					if (ext.Contains("png"))
					{
						Bitmap bmp1 = new Bitmap(_path);
						var quantizer = new WuQuantizer();
						using (var quantized = quantizer.QuantizeImage(bmp1))
						{
							quantized.Save(targetFile, ImageFormat.Png);
						}
					}
					else
					{
						System.Drawing.Imaging.Encoder myEncoder =
							System.Drawing.Imaging.Encoder.Quality;
						EncoderParameters myEncoderParameters = new EncoderParameters(1);

						long imageRatio = Convert.ToInt64(ConfigurationManager.AppSettings["ImageRatio"].ToString());

						EncoderParameter myEncoderParameter = new EncoderParameter(myEncoder, imageRatio);
						myEncoderParameters.Param[0] = myEncoderParameter;
						Bitmap bmp1 = new Bitmap(strm);
						bmp1.Save(targetFile, jpgEncoder, myEncoderParameters);
					}
					
					
					
				}
				ViewBag.Message = "File Uploaded Successfully!!";
			}
			catch(Exception ex)
			{
				logger.Error(string.Format("UploadFile - Error: {0} {1} Stack: {2} {1} Inner Exception: {3} "
					, ex.Message
					, Environment.NewLine
					, ex.StackTrace, ex.InnerException == null ? "" : ex.InnerException.Message));
			}

			return RedirectToAction("ShowImages", "Home", new { fileName = file.FileName });
		}

		private ImageCodecInfo GetEncoder(ImageFormat format)
		{
			ImageCodecInfo[] codecs = ImageCodecInfo.GetImageDecoders();
			foreach (ImageCodecInfo codec in codecs)
			{
				if (codec.FormatID == format.Guid)
				{
					return codec;
				}
			}
			return null;
		}


		public ActionResult ShowImages(string fileName)
		{
			if(string.IsNullOrWhiteSpace(fileName))
				return RedirectToAction("Index", "Home");

			int decimalPlaces = Convert.ToInt32(ConfigurationManager.AppSettings["DecimalPlaces"].ToString());
			List<ImageInfo> images = new List<ImageInfo>();
			double sourceMbs = 0;
			double compMbs = 0;
			try
			{
				var tempFileName = string.Concat(Path.GetFileName(fileName));
				var targetFile = Path.Combine(Server.MapPath("~/UploadedFiles"), tempFileName);
				if(System.IO.File.Exists(targetFile))
				{
					FileInfo fileInfo = new FileInfo(targetFile);
					sourceMbs = Math.Round(ConvertBytesToMegabytes(fileInfo.Length), decimalPlaces);
					ImageInfo image = new ImageInfo
					{
						ImageName = tempFileName,
						ImagePath = Server.MapPath("~/UploadedFiles"),
						ImageSizeInfo = sourceMbs.ToString() + " MB"
					};
					images.Add(image);
				}

				string compressedFileName = string.Concat(Path.GetFileNameWithoutExtension(fileName), "_Compressed", Path.GetExtension(fileName));

				targetFile = Path.Combine(Server.MapPath("~/UploadedFiles"), compressedFileName);

				if (System.IO.File.Exists(targetFile))
				{
					FileInfo fileInfo = new FileInfo(targetFile);
					compMbs = Math.Round(ConvertBytesToMegabytes(fileInfo.Length), decimalPlaces);
					double diff = (sourceMbs - compMbs);
					double prct = Math.Round(diff / sourceMbs * 100, decimalPlaces);
					ImageInfo image = new ImageInfo
					{
						ImageName = compressedFileName,
						ImagePath = Server.MapPath("~/UploadedFiles"),
						ImageSizeInfo = compMbs.ToString() + " MB",
						Savings = prct.ToString() + " %"
					};
					images.Add(image);
				}
			}
			catch (Exception ex)
			{
				logger.Error(string.Format("UploadFile - Error: {0} {1} Stack: {2} {1} Inner Exception: {3} "
					, ex.Message
					, Environment.NewLine
					, ex.StackTrace, ex.InnerException == null ? "" : ex.InnerException.Message));
			}
			if (compMbs >= sourceMbs)
				return View("Errors");

			return View(images);
		}

		public ActionResult Errors()
		{
			return View();
		}

		private double ConvertBytesToMegabytes(long bytes)
		{
			return (bytes / 1024f) / 1024f;
		}

		public ActionResult About()
		{
			ViewBag.Message = "Your application description page.";

			return View();
		}

		public ActionResult Contact()
		{
			ViewBag.Message = "Your contact page.";

			return View();
		}

		private void ReduceImageSize(double scaleFactor, Stream sourcePath, string targetPath)
		{
			using (var image = System.Drawing.Image.FromStream(sourcePath))
			{
				var newWidth = (int)(image.Width * scaleFactor);
				var newHeight = (int)(image.Height * scaleFactor);
				var thumbnailImg = new Bitmap(newWidth, newHeight);
				var thumbGraph = Graphics.FromImage(thumbnailImg);
				thumbGraph.CompositingQuality = CompositingQuality.HighQuality;
				thumbGraph.SmoothingMode = SmoothingMode.HighQuality;
				thumbGraph.InterpolationMode = InterpolationMode.HighQualityBicubic;
				var imageRectangle = new Rectangle(0, 0, newWidth, newHeight);
				thumbGraph.DrawImage(image, imageRectangle);
				thumbnailImg.Save(targetPath, image.RawFormat);
			}
		}

	}
}